// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using NUnit.Framework;
using System.Data;
using Mono.Data.Sqlite;
using WideMargin.Database;
using WideMargin.MVCInterfaces;

namespace WideMargin.Database.Tests
{
	[TestFixture]
	public class BibleDatabaseTests
	{
		public BibleDatabaseTests ()
		{
		}
		
		[Test]
		public void DecodeDaylyReading_OneChapter_ParsedCorrectly()
		{
			BibleDatabase bibleDatabase = new BibleDatabase();
			DaylyReading reading = bibleDatabase.DecodeDaylyReading("Gen 1");
			Assert.That(reading.Chapters, Is.EqualTo("Genesis 1"));
			Assert.That(reading.FirstVerse.Book, Is.EqualTo("Genesis"));
			Assert.That(reading.FirstVerse.Chapter, Is.EqualTo(1));
			Assert.That(reading.FirstVerse.VerseNumber, Is.EqualTo(1));
		}
		
		[Test]
		public void DecodeDaylyReading_TwoChapters_ParsedCorrectly()
		{
			BibleDatabase bibleDatabase = new BibleDatabase();
			DaylyReading reading = bibleDatabase.DecodeDaylyReading("Gen 1-2");
			Assert.That(reading.Chapters, Is.EqualTo("Genesis 1-2"));
			Assert.That(reading.FirstVerse.Book, Is.EqualTo("Genesis"));
			Assert.That(reading.FirstVerse.Chapter, Is.EqualTo(1));
			Assert.That(reading.FirstVerse.VerseNumber, Is.EqualTo(1));
		}
		
		[Test]
		public void DecodeDaylyReading_2ndBook_ParsedCorrectly()
		{
			BibleDatabase bibleDatabase = new BibleDatabase();
			DaylyReading reading = bibleDatabase.DecodeDaylyReading("2Tim 3");
			Assert.That(reading.Chapters, Is.EqualTo("2 Timothy 3"));
			Assert.That(reading.FirstVerse.Book, Is.EqualTo("2 Timothy"));
			Assert.That(reading.FirstVerse.Chapter, Is.EqualTo(3));
			Assert.That(reading.FirstVerse.VerseNumber, Is.EqualTo(1));
		}
		
		[Test]
		public void DecodeDaylyReading_VerseRange_ParsedCorrectly()
		{
			BibleDatabase bibleDatabase = new BibleDatabase();
			DaylyReading reading = bibleDatabase.DecodeDaylyReading("Psa 119:3-12");
			Assert.That(reading.Chapters, Is.EqualTo("Psalms 119:3-12"));
			Assert.That(reading.FirstVerse.Book, Is.EqualTo("Psalms"));
			Assert.That(reading.FirstVerse.Chapter, Is.EqualTo(119));
			Assert.That(reading.FirstVerse.VerseNumber, Is.EqualTo(3));
		}
		
		[Test]
		public void DecodeDaylyReading_2And3John()
		{
			BibleDatabase bibleDatabase = new BibleDatabase();
			DaylyReading reading = bibleDatabase.DecodeDaylyReading("2-3John");
			Assert.That(reading.Chapters, Is.EqualTo("2-3 John"));
			Assert.That(reading.FirstVerse.Book, Is.EqualTo("2 John"));
			Assert.That(reading.FirstVerse.Chapter, Is.EqualTo(1));
			Assert.That(reading.FirstVerse.VerseNumber, Is.EqualTo(1));
		}
	}
}

