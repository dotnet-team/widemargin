// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using WideMargin.MVCInterfaces;

namespace WideMargin.Database
{
	public class SearchPageResult
	{
		public SearchPageResult (IEnumerable<IVerse> verses, IDictionary<string, int> matches, int pageSize, int pageNumber, string searchText)
		{
			Verses = verses;
			Matches = matches;
			PageSize = pageSize;
			PageNumber = pageNumber;
			SearchText = searchText;
		}
		
		public IEnumerable<IVerse> Verses
		{
			get;
			private set;
		}
		
		public IDictionary<string, int> Matches
		{
			get;
			private set;
		}
		
		public int PageSize
		{
			get;
			private set;
		}
		
		public int PageNumber
		{
			get;
			private set;
		}
		
		public string SearchText
		{
			get;
			private set;
		}
	}
}

