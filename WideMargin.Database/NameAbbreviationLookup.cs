// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace WideMargin.Database
{
	/// <summary>
	/// Allows you to get the full book name from an abbrevation.
	/// </summary>
	public class NameAbbreviationLookup
	{
		private Dictionary<string,string> _nameLookup = new Dictionary<string, string>();
		
		/// <summary>
		/// Default constructor
		/// </summary>
		private NameAbbreviationLookup ()
		{
			_nameLookup = new Dictionary<string, string>();
			InitaliseNameLookup();
		}
		
		private static NameAbbreviationLookup _instance;
		
		/// <summary>
		/// Gets the NameAbbreviationLookup
		/// </summary>
		/// <returns>object which allows looking up books by abbreviation</returns>
		internal static NameAbbreviationLookup Get()
		{
			if(_instance == null)
			{
				_instance = new NameAbbreviationLookup();	
			}
			
			return _instance;
		}
		
		/// <summary>
		/// Given a abbreviated book name return the full name.
		/// </summary>
		/// <param name="name">abbreviated book name</param>
		/// <returns>full book name</returns>
		public string Lookup(string name)
		{
			string fullName;
			if(!_nameLookup.TryGetValue(name, out fullName))
			{
				string message = string.Format("Name abbrivation not recognized: {0}", name);
				throw new ArgumentException(message);
			}
			return fullName;
		}
		
		public bool TryLookup(string name, out string fullname)
		{
			return _nameLookup.TryGetValue(name, out fullname);
		}
		
		/// <summary>
		/// Initalises the lookup dictionary.
		/// </summary>
		private void InitaliseNameLookup()
		{
			_nameLookup.Add("Ge", "Genesis");
			_nameLookup.Add("Gen", "Genesis");
			_nameLookup.Add("Exo", "Exodus");
			_nameLookup.Add("Lev", "Leviticus");
			_nameLookup.Add("Num", "Numbers");
			_nameLookup.Add("Deu", "Deuteronomy");
			_nameLookup.Add("Josh", "Joshua");
			_nameLookup.Add("Jos", "Joshua");
			_nameLookup.Add("Jdgs", "Judges");
			_nameLookup.Add("Ruth", "Ruth");
			_nameLookup.Add("Rut", "Ruth");
			_nameLookup.Add("1Sm", "1 Samuel");
			_nameLookup.Add("1Sam", "1 Samuel");
			_nameLookup.Add("2Sm", "2 Samuel");
			_nameLookup.Add("2Sam", "2 Samuel");
			_nameLookup.Add("1Ki", "1 Kings");
			_nameLookup.Add("1Kin", "1 Kings");
			_nameLookup.Add("2Ki", "2 Kings");
			_nameLookup.Add("2Kin", "2 Kings");
			_nameLookup.Add("1Chr", "1 Chronicles");
			_nameLookup.Add("2Chr", "2 Chronicles");
			_nameLookup.Add("Ezr", "Ezra");
			_nameLookup.Add("Ezra", "Ezra");
			_nameLookup.Add("Neh", "Nehemiah");
			_nameLookup.Add("Est", "Esther");
			_nameLookup.Add("Job", "Job");
			_nameLookup.Add("Psa", "Psalms");
			_nameLookup.Add("Prv", "Proverbs");
			_nameLookup.Add("Prov", "Proverbs");
			_nameLookup.Add("Eccl", "Ecclesiastes");
			_nameLookup.Add("Ecc", "Ecclesiastes");
			_nameLookup.Add("SSol", "Song of Solomon");
			_nameLookup.Add("Song", "Song of Solomon");
			_nameLookup.Add("Isa", "Isaiah");
			_nameLookup.Add("Jer", "Jeremiah");
			_nameLookup.Add("Lam", "Lamentations");
			_nameLookup.Add("Eze", "Ezekiel");
			_nameLookup.Add("Dan", "Daniel");
			_nameLookup.Add("Hos", "Hosea");
			_nameLookup.Add("Joel", "Joel");
			_nameLookup.Add("Joe", "Joel");
			_nameLookup.Add("Amos", "Amos");
			_nameLookup.Add("Amo", "Amos");
			_nameLookup.Add("Obad", "Obadiah");
			_nameLookup.Add("Oba", "Obadiah");
			_nameLookup.Add("Jonah", "Jonah");
			_nameLookup.Add("Jon", "Jonah");
			_nameLookup.Add("Mic", "Micah");
			_nameLookup.Add("Nahum", "Nahum");
			_nameLookup.Add("Nah", "Nahum");
			_nameLookup.Add("Hab", "Habakkuk");
			_nameLookup.Add("Zep", "Zephaniah");
			_nameLookup.Add("Hag", "Haggai");
			_nameLookup.Add("Zec", "Zechariah");
			_nameLookup.Add("Mal", "Malachi");
			_nameLookup.Add("Mat", "Matthew");
			_nameLookup.Add("Mark", "Mark");
			_nameLookup.Add("Mar", "Mark");
			_nameLookup.Add("Luke", "Luke");
			_nameLookup.Add("Luk", "Luke");
			_nameLookup.Add("John", "John");
			_nameLookup.Add("Joh", "John");
			_nameLookup.Add("Acts", "Acts");
			_nameLookup.Add("Act", "Acts");
			_nameLookup.Add("Rom", "Romans");
			_nameLookup.Add("1Cor", "1 Corinthians");
			_nameLookup.Add("2Cor", "2 Corinthians");
			_nameLookup.Add("Gal", "Galatians");
			_nameLookup.Add("Eph", "Ephesians");
			_nameLookup.Add("Phi", "Philippians");
			_nameLookup.Add("Phil", "Philippians");
			_nameLookup.Add("Col", "Colossians");
			_nameLookup.Add("1Th", "1 Thessalonians");
			_nameLookup.Add("1The", "1 Thessalonians");
			_nameLookup.Add("1Thes", "1 Thessalonians");
			_nameLookup.Add("2Th", "2 Thessalonians");
			_nameLookup.Add("2The", "2 Thessalonians");
			_nameLookup.Add("2Thes", "2 Thessalonians");
			_nameLookup.Add("1Tim", "1 Timothy");
			_nameLookup.Add("2Tim", "2 Timothy");
			_nameLookup.Add("Titus", "Titus");
			_nameLookup.Add("Tit", "Titus");
			_nameLookup.Add("Phmn", "Philemon");
			_nameLookup.Add("Heb", "Hebrews");
			_nameLookup.Add("Jas", "James");
			_nameLookup.Add("Jam", "James");
			_nameLookup.Add("1Pet", "1 Peter");
			_nameLookup.Add("2Pet", "2 Peter");
			_nameLookup.Add("1Jn", "1 John");
			_nameLookup.Add("1Joh", "1 John");
			_nameLookup.Add("2Jn", "2 John");
			_nameLookup.Add("2Joh", "2 John");
			_nameLookup.Add("3Jn", "3 John");
			_nameLookup.Add("3Joh", "3 John");
			_nameLookup.Add("Jude", "Jude");
			_nameLookup.Add("Rev", "Revelation");
		}
	}
}

