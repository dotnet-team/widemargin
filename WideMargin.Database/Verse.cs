// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Data;
using WideMargin.MVCInterfaces;

namespace WideMargin.Database
{
	public class Verse : IVerse
	{
		internal Verse (string book, int chaper, int verseNumber, string contents)
		{
			Book = book;
			Chapter = chaper;
			VerseNumber = verseNumber;
			Contents = contents;
			Identifier = new VerseIdentifier(Book, Chapter, VerseNumber);
		}
		
		internal Verse(IDataReader reader)
		{
			int bookIndex = reader.GetOrdinal("Book");
			Book = reader.GetString(bookIndex);
			
			int chapterIndex = reader.GetOrdinal("Chapter");
			Chapter = reader.GetInt32(chapterIndex);
			
			int verseIndex = reader.GetOrdinal("VerseNumber");
			VerseNumber = reader.GetInt32(verseIndex);
			
			int contentsIndex = reader.GetOrdinal("Contents");
		    Contents = reader.GetString(contentsIndex);
			Identifier = new VerseIdentifier(Book, Chapter, VerseNumber);
		}
		
		public string Book
		{
			get;
			private set;
		} 
		
		public int Chapter
		{
			get;
			private set;
		}
		
		public int VerseNumber
		{
			get;
			private set;
		}
		
		public string Contents
		{
			get;
			private set;
		}
		
		public IVerseIdentifier Identifier
		{
			get;
			private set;
		}
	}
}

