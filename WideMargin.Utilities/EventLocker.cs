// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace WideMargin.Utilities
{
	/// <summary>
	/// Handles getting locks for adding and removing
	/// event subscriptions.
	/// </summary>
	public class EventLocker
	{
		public EventLocker ()
		{
		}
		
		/// <summary>
		/// Adds an even subscription in a thread safe manor
		/// </summary>
		/// <param name="eventHander">event to subscrib to.</param>
		/// <param name="value">Handler to add to event</param>
		public void Add<T>(ref EventHandler<T> eventHander, EventHandler<T> value) where T : EventArgs
		{
			lock(this)
			{
				eventHander += value;
			}
		}
		
		/// <summary>
		/// removes an even subscription in a thread safe manor
		/// </summary>
		/// <param name="eventHander">event to remove subscription from.</param>
		/// <param name="value">Handler to remove from event</param>
		public void Remove<T>(ref EventHandler<T> eventHander, EventHandler<T> value) where T : EventArgs
		{
			lock(this)
			{
				eventHander -= value;
			}
		}
	}
}

