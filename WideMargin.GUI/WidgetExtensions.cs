// WideMargin. Simple fast bible software.
// Copyright (C) 2011 Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Threading;
using Gtk;

namespace WideMargin.GUI
{
	public static class WidgetExtensions
	{
		private volatile static Thread _uiThread;
		
		/// <summary>
		/// Initializes the invoke.
		/// You must call this once from your UI thread on startup before your first call to Invoke
		/// </summary>
		public static void InitializeInvoke(this Gtk.Widget widget)
		{
			_uiThread = Thread.CurrentThread;
		}
		
		/// <summary>
		/// Determines whether we need to invoke to get onto the UI thread
		/// </summary>
		public static bool InvokeRequired(this Gtk.Widget widget)
		{
			if(Thread.CurrentThread == _uiThread)
			{
				return false;
			}
			return true;
		}
		
		/// <summary>
		/// Runs the action on the UI thread,
		/// Optimized to only invoke if we are not already on the UI thread.
		/// </summary>
		public static void Invoke(this Gtk.Widget widget, System.Action action)
		{
			if(widget.InvokeRequired())
			{
				Application.Invoke((_,__) =>
				{
					action();
				});
				return;
			}
			action();
		}
	}
}

