// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using Gtk;
using WideMargin.MVCInterfaces;
using Gdk;

namespace WideMargin.GUI
{
	[System.ComponentModel.ToolboxItem(true)]
	public partial class FilteredView : Gtk.Bin, IFilteredView, IGenericView
	{
		private RichTextView _textView;
		private IEnumerable<Filter> _filters;
		private VBox _filtersBox;
		
		public FilteredView ()
		{
			this.Build ();
			
			HBox hbox = new HBox();
			
			_textView = new RichTextView();
			_textView.VerseLinkClicked += HandleTextViewVerseLinkClicked;
			_textView.ActionLinkClicked += HandleTextViewActionLinkClicked;
			_textView.ScrollbarAtBottom += HandleTextViewScrollbarAtBottom;
			EventBox eventBox = new EventBox();
			eventBox.ModifyBg(StateType.Normal, new Color(255,255,255));
			eventBox.Add(hbox);
			hbox.Add(_textView);
			
			_filtersBox = new VBox();
			_filtersBox.ModifyBg(StateType.Normal, new Color(255,255,255));
			
			//add filters area
			var scrolledWindow = new ScrolledWindow();
			scrolledWindow.BorderWidth = 0;
			scrolledWindow.HscrollbarPolicy = PolicyType.Never;
			scrolledWindow.ModifyBg(StateType.Normal, new Color(255,255,255));
			Viewport viewport = new Viewport();
			viewport.Add (_filtersBox);
			viewport.ModifyBg(StateType.Normal, new Color(255,255,255));
			viewport.BorderWidth = 0;
			viewport.ShadowType = ShadowType.None;
			scrolledWindow.Add(viewport);
			hbox.PackStart(scrolledWindow, false,false, 0 );
			
			this.Add(eventBox);
			this.ModifyBg(StateType.Normal, new Color(255,255,255));
		}
		
		public bool VScrollbarVisible
		{
			get
			{
				return _textView.VScrollbarVisible;	
			}
		}
		
		protected virtual void HandleTextViewScrollbarAtBottom (object sender, EventArgs e)
		{
		}

		private void HandleTextViewActionLinkClicked (object sender, LinkClickedEventArgs<string> e)
		{
			if(VerseLinkClicked != null)
			{
				ActionLinkClicked.Invoke(this, e);
			}
		}

		private void HandleTextViewVerseLinkClicked (object sender, LinkClickedEventArgs<IVerseIdentifier> e)
		{
			if(VerseLinkClicked != null)
			{
				VerseLinkClicked.Invoke(this, e);
			}
		}
		
		public string Text
		{
			set
			{
				_textView.Text = value;	
			}
		}
		
		public void AppendText(string text)
		{
			_textView.AppendText(text);
		}
		
		public void ClearText()
		{
			_textView.Clear();
		}
		
		/// <summary>
		/// Gets or sets the filters.
		/// </summary>
		public IEnumerable<Filter> Filters
		{
			set
			{
				this.Invoke(() =>
				{
					SetFiltersUnsafe(value);
				});
			}
			get
			{
				return _filters;	
			}
		}

		protected void SetFiltersUnsafe(IEnumerable<Filter> filters)
		{
			foreach(Widget child in _filtersBox.Children)
			{
				_filtersBox.Remove(child);	
			}
			
			_filters = filters;
			
			foreach(var filter in _filters)
			{
				System.Action view = filter.PerformFilter;
				Button button = new Button(new Label(filter.Name));
				_filtersBox.PackStart(button,false,false,0);
				_filtersBox.BorderWidth = 4;
				button.Clicked += (sender, args) => view();
			}
			ShowAll ();
		}

		public event EventHandler<LinkClickedEventArgs<IVerseIdentifier>> VerseLinkClicked;

		public event EventHandler<LinkClickedEventArgs<string>> ActionLinkClicked;
	}
}

