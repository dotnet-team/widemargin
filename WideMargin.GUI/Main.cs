// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Gtk;
using WideMargin.MVCInterfaces;
using WideMargin.Utilities;

namespace WideMargin.GUI
{
	public class GTKWideMarginView : IWideMarginView
	{
		private MainWindow _mainWindow;
		private EventLocker _eventLock = new EventLocker();
		private EventHandler<EventArgs> _newTabRequest;
		
		public GTKWideMarginView()
		{
			Application.Init ();
			_mainWindow = new MainWindow();
			_mainWindow.NewTabRequest += HandleMainWindowNewTabRequest;
		}

		private void HandleMainWindowNewTabRequest (object sender, EventArgs e)
		{
			_newTabRequest.Fire(this, e);
		}
		
		public void Run()
		{
			_mainWindow.InitializeInvoke();
			_mainWindow.Show ();
			Application.Run ();
		}

		public IBibleView CreateBibleView(bool show)
		{
			return _mainWindow.AddTab(show);	
		}
		
		public event EventHandler<EventArgs> NewTabRequest
		{
			add
			{
				_eventLock.Add(ref _newTabRequest, value);
			}
			remove
			{
				_eventLock.Remove(ref _newTabRequest, value);
			}
		}
		
		public void Dispose()
		{
			//nothing to do yet	
		}
	}
}

