// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Gtk;
using System.Text;

namespace WideMargin.GUI
{
	/// <summary>
	/// Scrolled window which doesn't have a gap
	/// between the scroll bar and content.
	/// </summary>
	public class GaplessScrolledWindow : ScrolledWindow
	{
		private const string _style = "WideMargin.GUI.GaplessScrolledWindow";

		static GaplessScrolledWindow ()
		{
			var rcBuilder = new StringBuilder();
			rcBuilder.AppendFormat("style \"{0}\"{1}", 
			                       _style,
			                       Environment.NewLine);
			rcBuilder.AppendLine(@"{");
			rcBuilder.AppendLine(@"	 GtkScrolledWindow::scrollbar-spacing = 0");
			rcBuilder.AppendLine(@"}");
			
			Rc.ParseString(rcBuilder.ToString());
		}

		/// <summary>
		/// Creates Gapless ScrollWindow
		/// </summary>
		public GaplessScrolledWindow () 
			: base ()
		{
			var rc = string.Format ("widget \"*.{0}\" style \"{1}\" ", 
			                        Name, 
			                        _style);
			Gtk.Rc.ParseString (rc);
		}
	}
}

