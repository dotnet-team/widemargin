// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace WideMargin.MVCInterfaces
{
	/// <summary>
	/// Interface for a view which shows search results.
	/// </summary>
	public interface ISearchResultsView : IFilteredView
	{
		/// <summary>
		/// Clears out all the displayed results
		/// </summary>
		void Clear();
		
		/// <summary>
		/// Appends results to the end of the view.
		/// </summary>
		/// <param name='verses'>Verses.</param>
		void AppendResults(IEnumerable<IVerse> verses);

		void SetResults(IEnumerable<IVerse> verses, IEnumerable<Filter> filters);
		
		/// <summary>
		/// Occurs when the view requires more search results
		/// </summary>
		event EventHandler<EventArgs> MoreResultsRequired;
	}
}

