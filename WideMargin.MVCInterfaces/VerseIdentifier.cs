// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using WideMargin.MVCInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace WideMargin.MVCInterfaces
{
	/// <summary>
	/// Uniquely identifier a bible verse
	/// </summary>
	public class VerseIdentifier : IVerseIdentifier
	{
		/// <summary>
		/// Verse identifier constructor
		/// </summary>
		/// <param name="book">full book name</param>
		/// <param name="chapter">chapter number</param>
		/// <param name="verseNumber">verse number</param>
		public VerseIdentifier(string book, int chapter, int verseNumber)
		{
			Book = book;
			Chapter = chapter;
			VerseNumber = verseNumber;
		}
		
		/// <summary>
		/// Constructs a Verse Id from a standard verse string like "Genesis 5:3"
		/// </summary>
		/// <param name="verse">verse</param>
		public VerseIdentifier(string verse)
		{
			string[] parts = verse.Split(':');
			
			VerseNumber = int.Parse(parts[1]); 
			
			string chapterBook = parts[0];
			
			Chapter = int.Parse(chapterBook.Split(' ').Last());
			Book = chapterBook.Substring(0, chapterBook.LastIndexOf(' '));
			
		}
		
		/// <summary>
		/// full book name
		/// </summary>
		public string Book
		{
			get;
			private set;
		} 
		
		/// <summary>
		/// Chapter number
		/// </summary>
		public int Chapter
		{
			get;
			private set;
		}
		
		/// <summary>
		/// Verse number
		/// </summary>
		public int VerseNumber
		{
			get;
			private set;
		}
		
		/// <summary>
		/// prints standard verse format
		/// </summary>
		/// <returns>human readable verse identifier.</returns>
		public override string ToString ()
		{
			return string.Format ("{0} {1}:{2}", Book, Chapter, VerseNumber);
		}
	}
}

