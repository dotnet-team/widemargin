// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace WideMargin.MVCInterfaces
{
	public interface IBibleView
	{
		event EventHandler<EventArgs> NavigateBackwards;
		
		event EventHandler<EventArgs> NavigateForwards;
		
		event EventHandler<StringEventArgs> SearchRequest;
		
		event EventHandler<EventArgs> RequestAutoCompleteList;
		
		/// <summary>
		/// User has requested about info to be shown in
		/// an about box or similar.
		/// </summary>
		event EventHandler<EventArgs> RequestAboutInfo;
		
		IPassageView PassageView
		{
			get;
		}
		
		ISearchResultsView SearchView
		{
			get;	
		}
		
		IGenericView GenericView
		{
			get;
		}
		
		IFilteredView BooksView
		{
			get;
		}
		
		void ShowPassageView();
		
		void ShowSearchView();
		
		/// <summary>
		/// Shows the GenericView, this is used for the dayly readings.
		/// </summary>
		void ShowGenericView();
		
		void ShowBooksView();
		
		void ChangeTitle(string title);
		
		void ChangeBibleBar(string text);
		
		void ShowAutoCompleteList(IEnumerable<string> list);
		
		/// <summary>
		/// Show about info
		/// </summary>
		/// <param name="text">About info including XML markup.</param>
		void ShowAboutInfo(string text);

		/// <summary>
		/// Runs the action on the UI thread
		/// </summary>
		/// <param name='action'>
		/// Action to run.
		/// </param>
		void RunOnUiThread(Action action);
	}
}

