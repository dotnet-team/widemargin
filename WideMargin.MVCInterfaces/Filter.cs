// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace WideMargin.MVCInterfaces
{
	/// <summary>
	/// A filter for use in a filtered view
	/// </summary>
	public class Filter
	{
		private readonly Action _filterAction;
		private readonly string _name;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="WideMargin.MVCInterfaces.Filter"/> class.
		/// </summary>
		/// <param name='name'>
		/// Name to put on the filter button
		/// </param>
		/// <param name='filterAction'>
		/// Action to perform when the filter is activated
		/// </param>
		public Filter (string name, Action filterAction)
		{
			_filterAction = filterAction;
			_name = name;
		}
		
		/// <summary>
		/// Name to put on the filter button
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}
		
		/// <summary>
		/// Performs the filter action
		/// </summary>
		public void PerformFilter()
		{
			_filterAction();
		}
	}
}

