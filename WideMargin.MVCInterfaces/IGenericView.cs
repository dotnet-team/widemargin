// WideMargin. Simple fast bible software.
// Copyright (C) 2011  Daniel Hughes
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

namespace WideMargin.MVCInterfaces
{
	/// <summary>
	/// Interface for view which can display generic text with markup.
	/// </summary>
	public interface IGenericView
	{
		/// <summary>
		/// Text including markup
		/// </summary>
		string Text
		{
			set;	
		}
		
		/// <summary>
		/// Occurs when a verse link is clicked.
		/// </summary>
		event EventHandler<LinkClickedEventArgs<IVerseIdentifier>> VerseLinkClicked;
		
		/// <summary>
		/// Occurs when a action link is clicked.
		/// </summary>
		event EventHandler<LinkClickedEventArgs<string>> ActionLinkClicked;
	}
}

